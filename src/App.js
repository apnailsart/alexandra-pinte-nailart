import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { GDPRNotification } from "./Components/gdpr-notification/gdpr-notification";
import Homepage from "./Pages/homepage/homepage.js";
import Gallery from "./Pages/gallery/gallery.js";
import About from "./Pages/about/about_me.js";
import Services from "./Pages/services/services.js";
import Appointment from "./Pages/appointment/appointment.js";

import ContactMe from "./Pages/contact/contact.js";

function App() {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/gallery/:filter?">
            <Gallery />
          </Route>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/services">
            <Services />
          </Route>
          <Route path="/appointment">
            <Appointment />
          </Route>
          <Route path="/contact">
            <ContactMe />
          </Route>
          <Route path="/">
            <Homepage />
          </Route>
        </Switch>
      </Router>
      <GDPRNotification />
    </>
  );
}

export default App;
