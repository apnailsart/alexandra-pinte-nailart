import React from "react";
import { Menu32 } from "@carbon/icons-react";
import styles from "./mobile-menu.module.scss";
import { Link, useLocation } from "react-router-dom";

export function MobileMenu({ items }) {
  const { pathname } = useLocation();

  return (
    <>
    <div className="d-md-none py-5" />
    <div
      className={`${styles.container} row d-md-none px-4 align-items-center`}
    >
      <div className="col-12 d-flex align-items-center justify-content-start">
        <div className="mr-3">
          <input
            className={styles.menuInput}
            type="checkbox"
            id="mobile-menu"
          />
          <label className={`${styles.menuButton} py-3`} htmlFor="mobile-menu">
            <Menu32 />
          </label>
          <label className={styles.overlay} htmlFor="mobile-menu" />
          <ul className={`${styles.navigationList} py-5 px-0 m-0`}>
            {items?.map((item) => {
              const isActive = item.path === pathname || (pathname.indexOf(item.path) !== -1 && item.path !== '/');
              return (
                <li
                  key={item.path}
                  className={`pl-4 mb-3 ${styles.navigationItem} ${
                    isActive ? styles.active : ""
                  }`}
                >
                  <Link className={styles.navigationAnchor} to={item.path}>
                    {item.name}
                  </Link>
                  {item.children && (
                    <ul>
                      {item.children.map((childItem, childIndex) => (
                        <li
                          key={childIndex}
                          className={
                            pathname.indexOf(childItem.path) !== -1
                              ? styles.active
                              : ""
                          }
                        >
                          <Link to={childItem.path}>{childItem.name}</Link>
                        </li>
                      ))}
                    </ul>
                  )}
                </li>
              );
            })}
          </ul>
        </div>
        <div className="d-flex justify-content-center" style={{ flex: 1 }}>
          <img className={styles.logo} alt="logo" src="/images/logo-final.jpg"></img>
        </div>
      </div>
    </div>
    </>
  );
}

