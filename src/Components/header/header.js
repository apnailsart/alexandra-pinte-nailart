import React from "react";
import { Link, useLocation } from "react-router-dom";
import { MobileMenu } from "../mobile-menu/mobile-menu";
import "./header.css";

const leftItems = [
  {
    path: "/",
    name: "Home",
  },
  {
    path: "/about",
    name: "About",
  },
  {
    path: "/services",
    name: "Services",
  },
];

const rightItems = [
  {
    path: "/gallery",
    name: "Gallery",
    children: [
      {
        path: "/gallery/all",
        name: "See All",
      },
      {
        path: "/gallery/minimal",
        name: "Minimal",
      },
      {
        path: "/gallery/holiday",
        name: "Holiday Themed",
      },
      {
        path: "/gallery/daring",
        name: "Daring",
      },
    ],
  },
  {
    path: "/appointment",
    name: "Appointment",
  },
  {
    path: "/contact",
    name: "Contact",
  },
];

const menuItems = [...leftItems, ...rightItems];

function Header() {
  const { pathname } = useLocation();

  return (
    <>
      <div className="header row d-none pt-3 d-md-flex">
        {/*header*/}
        <ul className="navy">
          {leftItems.map((item, index) => (
            <li key={index} className={item.path === pathname ? "active" : ""}>
              <Link to={item.path}>{item.name}</Link>
            </li>
          ))}
        </ul>
        <Link className="logo" to="/">
          <img alt="logo" src="/images/logo-final.jpg" />
        </Link>
        <ul className="navy">
          {rightItems.map((item, index) => (
            <li
              key={index}
              className={pathname.indexOf(item.path) !== -1 ? "active" : ""}
            >
              <Link to={item.path}>{item.name}</Link>
              {item.children && (
                <ul>
                  {item.children.map((childItem, childIndex) => (
                    <li
                      key={childIndex}
                      className={
                        pathname.indexOf(childItem.path) !== -1 ? "active" : ""
                      }
                    >
                      <Link to={childItem.path}>{childItem.name}</Link>
                    </li>
                  ))}
                </ul>
              )}
            </li>
          ))}
        </ul>
      </div>
      <MobileMenu items={menuItems} />
    </>
  );
}

export default Header;
