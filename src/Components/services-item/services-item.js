import React from "react";

export function ServiceItem(props) {
  const title = props.title;
  const price = props.price;
  return (
    <li className="d-flex justify-content-between">
      <p className="services-title">{title}</p>
      <span className="services-price">{price}</span>
    </li>
  );
}
