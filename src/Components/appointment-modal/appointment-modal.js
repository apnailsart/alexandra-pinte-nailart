import React, { useState, useEffect, Fragment } from "react";
import { Modal } from "../modal/modal";
import DatePicker from "react-datepicker";
import styles from "./appointment-modal.module.scss";
import { Add20 } from "@carbon/icons-react";
import moment from "moment";
import { addNewAppointment, getCurrentAppointments } from "../../utils/api";

const services = {
  service1: {
    duration: 135,
    label: "Ojă semipermanentă",
  },
  service2: {
    duration: 165,
    label: "Construcție cu gel",
  },
  service3: {
    duration: 135,
    label: "Întreținere gel",
  },
  service4: {
    duration: 105,
    label: "Gel pe unghia naturală",
  },
  service5: {
    duration: 75,
    label: "Demontat manichiură cu gel",
  },
  service6: {
    duration: 75,
    label: "MENicure",
  },
};
const serviceDropdown = Object.keys(services).reduce((list, key) => {
  return [
    ...list,
    {
      value: key,
      name: services[key].label,
    },
  ];
}, []);

export function AppointmentModal({
  firstName,
  lastName,
  phoneNumber,
  validateFields,
}) {
  const [modal, setModal] = useState({
    isVisible: false,
    date: new Date(),
    service: "service1",
    disabledDates: null,
    errorMessage: "",
    successMessage: "",
  });

  useEffect(() => {
    let skipRequest = false;
    async function fetchDates() {
      try {
        const response = await getCurrentAppointments(14);
        const data = await response.json();
        if (!skipRequest) {
          setModal({
            ...modal,
            disabledDates: data,
          });
        }
      } catch (err) {
        console.log("Error: ", err);
      }
    }

    if (!modal.disabledDates) {
      fetchDates();
    }

    return () => {
      skipRequest = true;
    };
  }, [modal]);

  function openModal() {
    if (!firstName || !lastName || !phoneNumber) {
      validateFields();
    } else {
      setModal({
        ...modal,
        isVisible: true,
      });
    }
  }

  function closeModal() {
    setModal({
      isVisible: false,
      date: new Date(),
      service: "service1",
      disabledDates: null,
      errorMessage: "",
      successMessage: "",
    });
  }

  function onChange(ev, label) {
    const value = ev.currentTarget.value;
    setModal({
      ...modal,
      [label]: value,
    });
  }

  function onChangeDate(newDate) {
    setModal({
      ...modal,
      date: newDate,
    });
  }

  async function submitData() {
    try {
      const response = await addNewAppointment({
        start: modal.date.getTime(),
        summary: services[modal.service].label,
        duration: services[modal.service].duration,
        description: `<div><b>First Name: </b>${firstName}</div><div><b>Last Name: </b>${lastName}</div><div><b>Phone Number </b>${phoneNumber}</div>`,
      });
      const data = await response.json();
      if (data && data.status === "ok") {
        setModal({
          ...modal,
          successMessage: "Appointment Registered!",
        });
      }
      if (data && data.status === "occupied") {
        setModal({
          ...modal,
          errorMessage: "Occupied! Please select another date and time.",
        });
      }
    } catch (err) {
      console.log("Error", err);
    }
  }

  function checkDisabledDates(date, disabledDates) {
    let disabled = false;
    const dateNow = moment().subtract(1, "d");
    const day = moment(date).day();
    
    if (day === 0 || day === 6) {
      return styles.disabledDate;
    }
    if (disabledDates && disabledDates.length > 0) {
      for (let i = 0; i < disabledDates.length; i++) {
        const currentDate = disabledDates[i];
        const isBetween =
          moment(date).isBefore(dateNow) ||
          (moment(date).isAfter(currentDate.start) &&
            moment(date).isBefore(currentDate.end));
        if (isBetween) {
          disabled = true;
          break;
        }
      }
      return disabled ? styles.disabledDate : undefined;
    }
    return undefined;
  }

  function checkDisabledTimes(date, selectedDate, duration, disabledDates) {
    let disabled = false;
    const currentDate = moment(date);
    const pickedDay = moment(selectedDate);
    const dateNow = moment(
      new Date(
        pickedDay.year(),
        pickedDay.month(),
        pickedDay.date(),
        currentDate.hour(),
        currentDate.minute(),
        currentDate.second()
      )
    );
    const lowerLimitHour = new Date(
      pickedDay.year(),
      pickedDay.month(),
      pickedDay.date(),
      18,
      45,
      0
    ); 
    const higherLimitHour = new Date(
      pickedDay.year(),
      pickedDay.month(),
      pickedDay.date(),
      19,
      15,
      0
    );
    const fullDisabledDates = disabledDates.reduce((list, item) => {
      return [
        ...list,
        {
          start: moment(item.start).subtract(duration, "minutes"),
          end: item.start,
        },
        item,
      ];
    }, []);

    fullDisabledDates.push({
      end: lowerLimitHour,
      start: moment(lowerLimitHour.getTime()).subtract(19, "hours").valueOf(),
    });
    fullDisabledDates.push({
      start: higherLimitHour,
      end: moment(higherLimitHour.getTime()).add(10, "hours").valueOf(),
    });

    for (let i = 0; i < fullDisabledDates.length; i++) {
      const currentDate = fullDisabledDates[i];
      const isBetween =
        dateNow.isBefore(moment()) ||
        (dateNow.isSameOrAfter(currentDate.start) &&
          dateNow.isSameOrBefore(currentDate.end));
      if (isBetween) {
        disabled = true;
        break;
      }
    }
    return disabled ? styles.disabledDate : undefined;
  }

  return (
    <Fragment>
      <Modal
        isVisible={modal.isVisible}
        title="Add Appointment"
        onClose={closeModal}
      >
        {modal.errorMessage && (
          <div className={styles.formRow}>
            <div className={styles.successMessage}>{modal.errorMessage}</div>
          </div>
        )}
        {modal.successMessage && (
          <div className={styles.formRow}>
            <div className={styles.successMessage}>{modal.successMessage}</div>
          </div>
        )}
        <div className={styles.formRow}>
          <label>First Name: </label>
          <span>{firstName}</span>
        </div>
        <div className={styles.formRow}>
          <label>Last Name: </label>
          <span>{lastName}</span>
        </div>
        <div className={styles.formRow}>
          <label>Phone Number: </label>
          <span>{phoneNumber}</span>
        </div>
        <div className={styles.formRow}>
          <label>Select a service</label>
          <select
            className={styles.formField}
            value={modal.service}
            onChange={(ev) => onChange(ev, "service")}
          >
            {serviceDropdown.map((item, index) => (
              <option key={`${index}-${item.value}`} value={item.value}>
                {item.name}
              </option>
            ))}
          </select>
        </div>
        <div className={styles.formRow}>
          <label>Pick a date: </label>
          <DatePicker
            className={styles.date}
            selected={modal.date}
            onChange={onChangeDate}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={15}
            dateFormat="MMMM d, yyyy H:mm "
            dayClassName={(date) =>
              checkDisabledDates(date, modal.disabledDates)
            }
            timeClassName={(date) =>
              checkDisabledTimes(
                date,
                modal.date,
                services[modal.service].duration,
                modal.disabledDates
              )
            }
          />
        </div>
        <button className={styles.submitButton} onClick={submitData}>
          Submit appointment
        </button>
      </Modal>
      <button className={styles.appointmentButton} onClick={openModal}>
        <Add20 />
      </button>
    </Fragment>
  );
}
