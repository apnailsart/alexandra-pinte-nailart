import React, { useEffect, useState } from "react";
import styles from "./gdpr-notification.module.scss";

const gdprStorage = "alexandra-pinte-gdpr";

export function GDPRNotification() {
  const [showGDPR, setShowGDPR] = useState(true);

  useEffect(() => {
    const data = localStorage.getItem(gdprStorage);
    if (data) {
      setShowGDPR(false);
    }
  }, []);

  function onClick() {
    setShowGDPR(false);
    localStorage.setItem(gdprStorage, "checked");
  }

  return (
    showGDPR && (
      <div className={styles.gdpr}>
        <div className="container">
          <div className="row">
            <div className="col d-flex align-items-center">
              <p className={styles.textBlock}>
                Pentru scopuri precum afișarea de conținut personalizat, folosim
                module cookie sau tehnologii similare. Apăsând Accept sau
                navigând pe acest website, ești de acord să permiți colectarea
                de informații prin cookie-uri sau tehnologii similare. Află in
                sectiunea Politica de Cookies mai multe despre cookie-uri,
                inclusiv despre posibilitatea retragerii acordului.
              </p>
              <button className={styles.button} onClick={onClick}>
                Accept
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  );
}
