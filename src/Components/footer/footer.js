import React from "react";

export function Footer() {
  return (
    <div className="container d-flex justify-content-center">
      <a className="my-profile" href="https://alla.tech/">
        &copy; 2021. Alexandra Pinte Website - Designed by Alla
      </a>
    </div>
  );
}
