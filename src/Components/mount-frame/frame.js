import React from "react";

export function Frame(props) {
    let style = null
    if (props.width && props.height) {
        style = {width: props.width, height: props.height}
    }
    return (
        <div className="my-frame" style={style}>
            <span className="top-side"></span>
            <span className="bottom-side"></span>
            <span className="left-side"></span>
            <span className="right-side"></span>
            {props.children}
        </div>
    )
}