import React, { useState } from "react";
import Header from "../../Components/header/header";
import { AppointmentModal } from "../../Components/appointment-modal/appointment-modal";
import styles from "./appointment.module.scss";
import { Footer } from '../../Components/footer/footer';

function Appointment() {
  const [data, setData] = useState({
    firstName: "",
    lastName: "",
    phoneNumber: "",
    error: "",
  });

  function onChangeInput(event, label) {
    const value = event.currentTarget.value;
    if (
      (label === "phoneNumber" && /^\d+$/.test(value) && value.length <= 10) ||
      value === ""
    ) {
      setData({
        ...data,
        [label]: value,
        error: null,
      });
    } else if (
      (label !== "phoneNumber" && /^[a-zA-Z\s]+$/.test(value)) ||
      value === ""
    ) {
      setData({
        ...data,
        [label]: value,
        error: null,
      });
    }
  }

  function validateFields() {
    setData({
      ...data,
      error: "Please enter the client information",
    });
  }

  return (
    <div className="container-fluid">
      <Header />
      <div className="container">
        <div className={styles.appointmentWrapper}>
          <div className="row">
            <div className="col-12 col-md-8 offset-md-2 p-md-5">
              {data.error && (
                  <div className={`${styles.errorMessage} pb-3`}>{data.error}</div>
              )}
              <div className={styles.formRow}>
                <label className={styles.formLabel}>First Name:</label>
                <input
                  className={`${styles.formInput} ${!data.firstName && data.error ? styles.inputRequired : ''}`}
                  onChange={(event) => onChangeInput(event, "firstName")}
                  placeholder=""
                  value={data.firstName}
                />
              </div>
              <div className={styles.formRow}>
                <label className={styles.formLabel}>Last Name:</label>
                <input
                  className={`${styles.formInput} ${!data.lastName && data.error ? styles.inputRequired : ''}`}
                  onChange={(event) => onChangeInput(event, "lastName")}
                  placeholder=""
                  value={data.lastName}
                />
              </div>
              <div className={styles.formRow}>
                <label className={styles.formLabel}>Phone Number:</label>
                <input
                  className={`${styles.formInput} ${!data.phoneNumber && data.error ? styles.inputRequired : ''}`}
                  onChange={(event) => onChangeInput(event, "phoneNumber")}
                  placeholder=""
                  value={data.phoneNumber}
                />
              </div>
              <div className={`d-flex justify-align-between ${styles.event}`}>
                <span className={styles.eventText}>It's a date!</span>
                <AppointmentModal
                  firstName={data.firstName}
                  lastName={data.lastName}
                  phoneNumber={data.phoneNumber}
                  validateFields={validateFields}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
export default Appointment;
