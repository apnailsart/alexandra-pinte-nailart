import React from "react";
//import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./about_me.css";
import Header from "../../Components/header/header.js";
import { Frame } from "../../Components/mount-frame/frame.js";

class About extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <Header />
        <div className="container d-none d-md-block pb-5">
          <div className="frame-ab-wrapper mt-md-5">
            <Frame width="100%" height="60vw">
              <div className="row mb-5 about-wrapper">
                <div className="col-12 col-md-5 align-items-center pt-1 pb-2 image-collage-wrapper">
                  <img
                    src="images/FINAL.jpg"
                    className="img-thumbnail about-img"
                  />
                </div>
                <div className="col-12 col-md-7 text-collage-wrapper">
                  <h1 className="title-border">Hello there, gorgeous!</h1>
                  <div className="description-wrapper">
                    <p>
                      {" "}
                      Calatoria mea in acest domeniu de vis a inceput in 2011
                      cand am descoperit pentru prima data manichiura tehnica si
                      m-am indragostit, iremediabil!
                    </p>
                    <p>
                      {" "}
                      Mi-am petrecut anii de studentie acumuland cat mai multa
                      experienta si pornind o mica afacere “de acasa”. Imi
                      placea la nebunie, insa in 2013 am decis ca vreau sa
                      incerc si altceva, astfel ca am facut primii pasi in
                      domeniul “corporate”.
                    </p>
                    <p>
                      {" "}
                      De la agent servicii clienti, la Team Leader si pana la RO
                      Customer Service Manager, le-am facut pe toate. Am invatat
                      multe, FOARTE multe! Si cu toate ca dragostea mea pentru
                      lucrul cu oamenii nu se va sfarsi niciodata, imi e clar
                      acum, dupa atatia ani, ca nu va egala IN VECI pasiunea
                      pentru manichiura.
                    </p>
                    <p>
                      {" "}
                      Astfel ca, in Ianuarie 2020, am decis sa revin la ceea ce
                      imi doresc cu adevarat si am inceput sa pun bazele visului
                      meu. Nu, nu am renuntat inca la lumea corporatista, insa,
                      incetul cu incetul, dedic tot mai mult timp vietii in
                      salon.
                    </p>
                    <p>
                      {" "}
                      Daca iti doresti o manichiura curata, minimalista si de
                      bun gust, te astept cu drag sa ne cunoasteam si sunt mai
                      mult decat fericita sa fac parte din povestea ta.
                    </p>
                    <div className="description-dscr">
                      Love,
                      <br />
                      Alexandra
                    </div>
                  </div>
                </div>
              </div>
            </Frame>
          </div>
          
        </div>
        <div className="container frame-ab-wrapper d-md-none">
            <div className="row mb-5 align-items-center">
              <div className="col-12 col-md-5 align-items-center pt-1 pb-2 image-collage-wrapper">
                <img
                  src="images/FINAL.jpg"
                  className="img-thumbnail about-img"
                />
              </div>
              <div className="col-12 col-md-7 text-collage-wrapper">
                <h1 className="title-border">Hello there, gorgeous!</h1>
                <div className="description-wrapper">
                  <p>
                    {" "}
                    Calatoria mea in acest domeniu de vis a inceput in 2011 cand
                    am descoperit pentru prima data manichiura tehnica si m-am
                    indragostit, iremediabil!
                  </p>
                  <p>
                    {" "}
                    Mi-am petrecut anii de studentie acumuland cat mai multa
                    experienta si pornind o mica afacere “de acasa”. Imi placea
                    la nebunie, insa in 2013 am decis ca vreau sa incerc si
                    altceva, astfel ca am facut primii pasi in domeniul
                    “corporate”.
                  </p>
                  <p>
                    {" "}
                    De la agent servicii clienti, la Team Leader si pana la RO
                    Customer Service Manager, le-am facut pe toate. Am invatat
                    multe, FOARTE multe! Si cu toate ca dragostea mea pentru
                    lucrul cu oamenii nu se va sfarsi niciodata, imi e clar
                    acum, dupa atatia ani, ca nu va egala IN VECI pasiunea
                    pentru manichiura.
                  </p>
                  <p>
                    {" "}
                    Astfel ca, in Ianuarie 2020, am decis sa revin la ceea ce
                    imi doresc cu adevarat si am inceput sa pun bazele visului
                    meu. Nu, nu am renuntat inca la lumea corporatista, insa,
                    incetul cu incetul, dedic tot mai mult timp vietii in salon.
                  </p>
                  <p>
                    {" "}
                    Daca iti doresti o manichiura curata, minimalista si de bun
                    gust, te astept cu drag sa ne cunoasteam si sunt mai mult
                    decat fericita sa fac parte din povestea ta.
                  </p>
                  <div className="description-dscr">
                    Love,
                    <br />
                    Alexandra
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}

export default About;
