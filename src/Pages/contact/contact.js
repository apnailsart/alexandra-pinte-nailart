import React from "react";
import MapContainer from "../../google-map";
import Header from "../../Components/header/header.js";
import { Link } from "react-router-dom";
import styles from "./contact.module.scss";
import { Footer } from '../../Components/footer/footer';

function ContactMe() {
  return (
    <div
      className={`${styles.container} ${styles.contactBackground} container-fluid`}
    >
      <img
        src="/images/BackgroundFinal.png"
        className={styles.imgBkg}
        alt="Responsive image"
      />
      <div className={styles.overlay}></div>
      <Header />
      <div className="container">
        <div className={`${styles.row} row`}>
          <div className="offset-4 col-4 mt-5 p-3 d-flex justify-content-center">
            <div className={styles.outerCircle}>
              <div
                className={`${styles.innerCircle} d-flex align-items-center justify-content-center`}
              >
                <div className={styles.innerText}>Connect with me</div>
              </div>
            </div>
          </div>
          <div className={`${styles.overlayRhombus} row`}>
            <div className="offset-1 col-10">
              <div className={`${styles.rhombusRow} row`}>
                <div className="col-3 px-0">
                  <div class={styles.rhombus}></div>
                </div>
                <div className="col-3 px-0">
                  <div class={styles.rhombus}></div>
                </div>
                <div className="col-3 px-0">
                  <div class={styles.rhombus}></div>
                </div>
                <div className={`col-3 px-0 ${styles.lastRhombus}`}>
                  <div class={styles.rhombus}></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row d-none d-md-flex">
          <div className="offset-1 col-10">
            <div className={`${styles.rhombusRow} row`}>
              <div className="col-3 px-0">
                <div class={styles.rhombus}>
                  <div class={styles.rhombusText}>
                    Jud.Cluj,
                    <br /> Mun.Cluj-Napoca,
                    <br /> str. Dâmboviţei,
                    <br /> nr. 10
                  </div>
                </div>
              </div>
              <div className="col-3 px-0">
                <div class={styles.rhombus}>
                  <div class={styles.rhombusText}>Tel: 0757 415 384</div>
                </div>
              </div>
              <div className="col-3 px-0">
                <div class={styles.rhombus}>
                  <div class={styles.rhombusText}>
                    <div className="mb-5">
                      <a
                        className="no-link"
                        href="https://www.instagram.com/alexandrapinte.nailart"
                      >
                        Instagram
                      </a>
                    </div>
                    <div>
                      <a
                        className="no-link"
                        href="https://www.facebook.com/alexandrapinte.nailart/?ti=as"
                      >
                        Facebook
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className={`col-3 px-0 ${styles.lastRhombus}`}>
                <div class={styles.rhombus}>
                  <div class={styles.rhombusText}>
                    <h6>Get in touch</h6>
                    <Link to="/appointment" className="btn btn-sm heart-button">
                      <span style={{ fontSize: "5em", display: "block" }}>
                        <svg
                          width="2em"
                          height="1em"
                          viewBox="0 0 16 16"
                          class="bi bi-suit-heart-fill"
                          fill="currentColor"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"></path>
                        </svg>
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={`row d-md-none ${styles.mobileContact}`}>
          <div className="col-6 d-flex justify-content-center">
            Jud.Cluj,
            <br /> Mun.Cluj-Napoca, <br />str. Dâmboviţei,  nr. 10 <br />Tel: 0757 415 384
          </div>
          <div className="col-6 d-flex align-items-center justify-content-between flex-column">
            <a
              className="no-link"
              href="https://www.instagram.com/alexandrapinte.nailart"
            >
              Instagram
            </a>
            <a
              className="no-link"
              href="https://www.facebook.com/alexandrapinte.nailart/?ti=as"
            >
              Facebook
            </a>
          </div>
          <div className="col-12 mt-4 d-flex align-items-center justify-content-center">
            <h4>Get in touch</h4>
            <Link to="/appointment" className="btn btn-sm heart-button">
              <span style={{ fontSize: "5em", display: "block" }}>
                <svg
                  width="2em"
                  height="1em"
                  viewBox="0 0 16 16"
                  class="bi bi-suit-heart-fill"
                  fill="currentColor"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"></path>
                </svg>
              </span>
            </Link>
          </div>
        </div>
        <div className="row">
          <div className="offset-1 col-10 offset-md-3 col-md-6">
            <MapContainer width="auto" height="400px" />
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default ContactMe;
