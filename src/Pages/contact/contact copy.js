import React from "react";
import { Link } from "react-router-dom";
import MapContainer from "../../google-map";
import Header from "../../Components/header/header";
import "./contact.css";

function ContactMe() {
  return (
    <div className="container-fluid">
      <div className="container">
        <Header />
      </div>
      <div className="container mt-5 some-class">
        <div className="row justify-content-center connect-style">
          <div className="col-12 mb-5">
            <h4>Connect with me</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-6 info-wrapper">
            <p>Jud.Cluj, Mun.Cluj-Napoca, str. Dâmboviţei, nr. 10</p>
            <h5>Tel: 0757 415 384</h5>
            <div className="row pl-3">
              <a
                className="no-link"
                href="https://www.instagram.com/alexandrapinte.nailart"
              >
                Instagram
              </a>
              <a
                className="no-link"
                href="https://www.facebook.com/alexandrapinte.nailart/?ti=as"
              >
                Facebook
              </a>
            </div>
            <div className="row py-5 my-5">
              <div className="section-social-heart">
                <div className="row align-items-center">
                  <div className="col-12 section-heart">
                    <h2>Get in touch</h2>
                    <Link to="/appointment" className="btn btn-sm heart-button">
                      <span style={{ fontSize: "5em", display: "block" }}>
                        <svg
                          width="2em"
                          height="1em"
                          viewBox="0 0 16 16"
                          class="bi bi-suit-heart-fill"
                          fill="currentColor"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z"></path>
                        </svg>
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-6 section-title">
            <div className="row">
              <div className="col-12">
                <MapContainer width="auto" height="400px" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContactMe;
