import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { Carousel } from "react-responsive-carousel";
//import GoogleMapReact from 'google-map-react';
import MapContainer from "../../google-map";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./homepage.css";
import Header from "../../Components/header/header.js";
import { Frame } from "../../Components/mount-frame/frame.js";
import { Footer } from "../../Components/footer/footer";

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.figureRef = React.createRef();
    this.state = { angle: 0 };
  }

  gallerySpin(sign) {
    let newAngle = 0;
    if (!sign) {
      newAngle = this.state.angle + 45;
    } else {
      newAngle = this.state.angle - 45;
    }
    this.figureRef.current.setAttribute(
      "style",
      "-webkit-transform: rotateY(" +
        newAngle +
        "deg); transform: rotateY(" +
        newAngle +
        "deg);"
    );
    this.setState({
      angle: newAngle,
    });
  }

  render() {
    return (
      <Fragment>
        <div className="container-fluid">
          <Header />
          <div className="row">
            <div className="col-12 col-md-12 col-lg-12 inside-frame">
              <img
                src="/images/BackgroundFinal.png"
                className="img-fluid"
                alt="Responsive image"
              />
              <Frame>
                <div className="hometext-wrapper">
                  <h1 className="hometext">Dare to be different!</h1>
                </div>
                <div className="button-wrapper">
                  <Link to="/appointment" className="btn btn-lg my-button">
                    Let's meet!
                  </Link>
                </div>
              </Frame>
            </div>
          </div>
        </div>
        <div className="container">
          {/*body */}
          <div className="row mt-5">
            <div className="col-12 col-md-12 col-lg-12 mb-lg-3">
              <p className="h1 headline">TESTIMONIALS</p>
            </div>
            <div className="col-12 col-md-12 offset-lg-2 col-lg-8">
              <Carousel
                showIndicators={false}
                showArrows={true}
                infiniteLoop={true}
                showStatus={false}
                dynamicHeight={true}
              >
                <div>
                  <blockquote className="blockquote text-center testimonial">
                    <p className="mb-0">
                      Recomand cu cea mai mare încredere. Ale e foarte atentă la
                      detalii si lucreaza super îngrijit. Are mereu idei de
                      modele sau culori potrivite exact cu starea ta de spirit
                      atunci când ești în pana de idei.
                    </p>
                    <footer className="blockquote-footer">
                      Oana Marian{" "}
                      <cite title="Source Title">
                        recommends Alexandra Pinte - NailArt
                      </cite>
                    </footer>
                  </blockquote>
                </div>

                <div>
                  <blockquote class="blockquote text-center testimonial">
                    <p class="mb-0">
                      Recomand cu încredere, profesionalism, atentie, igiena si
                      talent! Lucreaza foarte bine, produsele folosite sunt de
                      calitate si durabile!
                    </p>
                    <footer class="blockquote-footer">
                      Ruxandra Mutulescu{" "}
                      <cite title="Source Title">
                        recommends Alexandra Pinte - NailArt
                      </cite>
                    </footer>
                  </blockquote>
                </div>
                <div>
                  <blockquote class="blockquote text-center testimonial">
                    <p class="mb-0">
                      Amazing nails.Up for a good nail challenge. She is classy
                      and fun! My nails never looked better!
                    </p>
                    <footer class="blockquote-footer">
                      Anamaria Dragla{" "}
                      <cite title="Source Title">
                        recommends Alexandra Pinte - NailArt
                      </cite>
                    </footer>
                  </blockquote>
                </div>
                <div>
                  <blockquote class="blockquote text-center testimonial">
                    <p class="mb-0">She's the best in town!</p>
                    <footer class="blockquote-footer">
                      Teofana Ioana Cismaș{" "}
                      <cite title="Source Title">
                        recommends Alexandra Pinte - NailArt
                      </cite>
                    </footer>
                  </blockquote>
                </div>
              </Carousel>
            </div>
          </div>
          <div className="row mt-5 pb-5 d-none d-md-flex">
            <div className="col-12 p-0">
              <div id="gallery">
                <figure id="spinner" ref={this.figureRef}>
                  <img src="images/BestWork1.jpg" alt="" />
                  <img src="images/BestWork2.jpg" alt="" />
                  <img src="images/BestWork3.jpg" alt="" />
                  <img src="images/BestWork4.jpg" alt="" />
                  <img src="images/BestWork5.jpg" alt="" />
                  <img src="images/BestWork6.jpg" alt="" />
                  <img src="images/BestWork7.jpg" alt="" />
                  <img src="images/BestWork8.jpg" alt="" />
                </figure>
                <span
                  className="controls left-arrow"
                  onClick={() => this.gallerySpin("-")}
                >
                  {"<"}
                </span>
                <span
                  className="controls right-arrow"
                  onClick={() => this.gallerySpin("")}
                >
                  {">"}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row d-md-none mb-3">
            <div className="col-6">
              <img className="home_image" src="images/BestWork1.jpg" alt="" />
            </div>
            <div className="col-6">
              <img className="home_image" src="images/BestWork2.jpg" alt="" />
            </div>
          </div>
          <div className="row d-md-none mb-3">
            <div className="col-6">
              <img className="home_image" src="images/BestWork3.jpg" alt="" />
            </div>
            <div className="col-6">
              <img className="home_image" src="images/BestWork4.jpg" alt="" />
            </div>
          </div>
          <div className="row d-md-none mb-3">
            <div className="col-6">
              <img className="home_image" src="images/BestWork5.jpg" alt="" />
            </div>
            <div className="col-6">
              <img className="home_image" src="images/BestWork6.jpg" alt="" />
            </div>
          </div>
          <div className="row d-md-none mb-3">
            <div className="col-6">
              <img className="home_image" src="images/BestWork7.jpg" alt="" />
            </div>
            <div className="col-6">
              <img className="home_image" src="images/BestWork8.jpg" alt="" />
            </div>
          </div>
        </div>
        <div className="container-fluid mt-5">
          <div className="row footer d-none d-md-flex">
            <div className="col-10 offset-1">
              <div className="row">
                <div className="col-4">
                  <MapContainer height="150px" width="300px" />
                </div>
                <div className="col-4">
                  <p class="phonenumber">Tel: 0757 415 384</p>
                </div>
                <div className="col-4">
                  <p class="lead">
                    <a
                      className="no-link"
                      href="https://www.facebook.com/alexandrapinte.nailart/?ti=as"
                    >
                      Facebook
                    </a>
                  </p>
                  <p class="lead">
                    <a
                      className="no-link"
                      href="https://www.instagram.com/alexandrapinte.nailart"
                    >
                      Instagram
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="container d-md-none">
            <div className="row d-md-none">
              <div className="col-6">
                <MapContainer height="150px" width="100%" />
              </div>
              <div className="col-6">
                <p class="lead">
                  <a className="no-link" href="tel:+40757-415-384">
                    Tel: 0757 415 384
                  </a>
                </p>
                <p class="lead">
                  <a
                    className="no-link"
                    href="https://www.facebook.com/alexandrapinte.nailart/?ti=as"
                  >
                    Facebook
                  </a>
                </p>
                <p class="lead">
                  <a
                    className="no-link"
                    href="https://www.instagram.com/alexandrapinte.nailart"
                  >
                    Instagram
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

export default Homepage;
