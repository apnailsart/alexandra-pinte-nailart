import React, { useState } from "react";
//import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./gallery.css";
import Header from "../../Components/header/header.js";
import GalleryModal from "../../Components/gallery-modal/gallery-modal";
import { useParams } from "react-router-dom";

const minimalImages = [
  "/images/gallery/received_318346389337673.jpeg",
  "/images/gallery/received_279838783281940.jpeg",
  "/images/gallery/received_378053263175826.jpeg",
  "/images/gallery/received_275969233474690.jpeg",
  "/images/sample1.jpg",
  "/images/gallery/04.jpg",
  "/images/gallery/05.jpg",
  "/images/gallery/09.jpg",
  "/images/gallery/received_327138824991488.jpeg",
  "/images/gallery/06.jpg",
  "/images/gallery/NEW17.jpg",
  "/images/gallery/NEW18.jpg",
  "/images/gallery/NEW19.jpg",
  "/images/gallery/NEW20.jpg",
  "/images/gallery/NEW13.jpg",
];

const holidayImages = [
  "/images/gallery/received_320285248996921.jpeg",
  "/images/gallery/received_343944806599927.jpeg",
  "/images/gallery/received_2715144375474630.jpeg",
  "/images/gallery/received_574371536576526.jpeg",
  "/images/gallery/03.jpg",
  "/images/gallery/NEW1.jpg",
  "/images/gallery/NEW2.jpg",
  "/images/gallery/NEW3.jpg",
  "/images/gallery/NEW5.jpg",
];

const daringImages = [
  "/images/gallery/received_305977690598592.jpeg",
  "/images/gallery/received_3166517990074957.jpeg",
  "/images/gallery/01.jpg",
  "/images/gallery/02.jpg",
  "/images/gallery/07.jpg",
  "/images/gallery/10.jpg",
  "/images/gallery/08.jpg",
  "/images/gallery/received_285346239343905.jpeg",
  "/images/gallery/received_205687284150981.jpeg",
  "/images/gallery/11.jpg",
  "/images/gallery/NEW6.jpg",
  "/images/gallery/NEW7.jpg",
  "/images/gallery/NEW8.jpg",
  "/images/gallery/NEW9.jpg",
  "/images/gallery/NEW10.jpg",
  "/images/gallery/NEW11.jpg",
  "/images/gallery/NEW12.jpg",
  "/images/gallery/NEW14.jpg",
  "/images/gallery/NEW15.jpg",
  "/images/gallery/NEW16.jpg",
];

const allImages = [...minimalImages, ...holidayImages, ...daringImages];

const filters = {
  'all': allImages,
  'minimal': minimalImages,
  'holiday': holidayImages,
  'daring': daringImages
}

function chunkArray(list) {
  let chunkCount = 3;
  const chunks = [];
  while (list.length) {
    const chunkSize = Math.ceil(list.length / chunkCount--);
    const chunk = list.slice(0, chunkSize);
    chunks.push(chunk);
    list = list.slice(chunkSize);
  }
  return {
    columnOne: chunks[0] || [],
    columnTwo: chunks[1] || [],
    columnThree: chunks[2] || [],
  };
}

function Gallery() {
  const { filter } = useParams();
  const [modal, setModal] = useState({
    currentImage: null,
    isVisible: false,
  });
  const imageData = chunkArray(filters[filter] || allImages);

  function closeModal() {
    setModal({
      currentImage: null,
      isVisible: false,
    });
  }

  function openModal(selectedImage) {
    setModal({
      currentImage: selectedImage,
      isVisible: true,
    });
  }

  return (
    <div className="container-fluid">
      <Header />
      <div className="container pt-5">
        <div className="row">
          <GalleryModal
            imageList={allImages}
            image={modal.currentImage}
            isVisible={modal.isVisible}
            onClose={closeModal}
          />
          <div className="col-12 col-md-3 px-0">
            {imageData.columnOne.map((image) => (
              <img
                key={image}
                className="gallery-image img-fluid"
                src={image}
                alt=""
                onClick={() => openModal(image)}
              />
            ))}
          </div>
          <div className="col-12 col-md-6 px-0">
            {imageData.columnTwo.map((image) => (
              <img
                key={image}
                className="gallery-image img-fluid"
                src={image}
                alt=""
                onClick={() => openModal(image)}
              />
            ))}
          </div>
          <div className="col-12 col-md-3 px-0">
            {imageData.columnThree.map((image) => (
              <img
                key={image}
                className="gallery-image img-fluid"
                src={image}
                alt=""
                onClick={() => openModal(image)}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default Gallery;
